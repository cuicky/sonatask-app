var myAppModule = angular.module('sonataskApp', [
    'ui.router',
    'angular-growl',
    'snap',
    'ui.sortable',
    'angularFileUpload',
    'ui.bootstrap'
]);

myAppModule.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/landing");
  //
  // Now set up the states
  $stateProvider
    .state('landing', {
      url: "/landing",
      templateUrl: "views/landing.html",
	  controller:"LandingCtrl as Landing"
    })
    .state('app', {
      url: "/app",
          abstract:true,
          templateUrl: "views/app/index.html",
          controller:"AppCtrl as App"
      })
    .state('app.notes', {
            url: "/notes",
          templateUrl: "views/app/notes.html",
          controller:"NotesCtrl as Notes"
    })
    .state('app.deletedNotes', {
      url: "/deletedNotes",
      templateUrl: "views/app/deletedNotes.html",
      controller:"DeletedNotesCtrl as DeletedNotes"
    })
    .state('app.todos', {
        url: "/app/todos",
        templateUrl: "views/app/todos.html",
        controller:"TodosCtrl as Todos"
    });
});
