'use strict';

/**
 * @ngdoc function
 * @name sonataskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sonataskApp
 */
angular.module('sonataskApp')
  .controller('LandingCtrl', function ($scope,$state,growl) {
    var self = this;

        // check if user is logged
        var currentUser = Parse.User.current();
        if (currentUser) {
            $state.go("app.notes");
            return;
        }

	$scope.mainView="signup";
        self.model={
            username:null,
            email:null,
            password:null
        }


        self.on={
            login:function(){
                $scope.mainView='login'
            },
            signup:function(){
                $scope.mainView='signup'
            },
            signUpUser:function(){
                var username =self.model.username;
                var email =self.model.email;
                var password =self.model.password;
                var user = new Parse.User();
                user.set("username", username);
                user.set("password", password);
                user.set("email", email);

                user.signUp(null, {
                    success: function(user) {
                        Parse.User.logIn(username,password, {
                            success: function(userLogged) {
                                // Do stuff after successful login.
                                $state.go("app.notes")

                            },
                            error: function(user, error) {
                                // The login failed. Check error to see why.
                            }
                        });

                    },
                    error: function(user, error) {
                        // Show the error message somewhere and let the user try again.
                        growl.error(error.message);
                        return error;
                    }
                });

            },
            logInUser:function(){
                var username =self.model.username;
                var password =self.model.password;
                Parse.User.logIn(username,password, {
                    success: function(userLogged) {
                        // Do stuff after successful login.
                        $state.go("app.notes")

                    },
                    error: function(user, error) {
                        // The login failed. Check error to see why.
                    }
                });
            }
        }
  });
