'use strict';

/**
 * @ngdoc function
 * @name sonataskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sonataskApp
 */
angular.module('sonataskApp')
  .controller('TodosCtrl', function ($scope,$state,growl) {
    var self = this;

        var currentUser = Parse.User.current();
        $scope.items=[];

        self.modelNote={
            title:null
        }

        self.on= {
            initialize: function () {
                $scope.tasks = [];
                var Note = Parse.Object.extend("Task");
                var query = new Parse.Query(Note);
                query.equalTo("user", currentUser);

                query.find({
                    success: function (userNotes) {
                        // userPosts contains all of the posts by the current user.
                        for (var i = 0; i < userNotes.length; i++) {
                            var objNote = {};
                            var object = userNotes[i];
                            objNote.id = object.id;
                            objNote.data = object.attributes
                            $scope.tasks.push(objNote)

                            $scope.$apply()
                        }
                    },
                    error: function (error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });
                console.log($scope.tasks)
            },
            saveNote: function () {
                console.log($scope.dt)
                var noteTitle = self.modelNote.title

                if(noteTitle == null){
                    growl.error("Please check form");
                    return
                }

                // Make a new post
                var Note = Parse.Object.extend("Task");
                var note = new Note();
                note.set("name", noteTitle);
                note.set("user", currentUser);
                note.set("check", false);
                note.set("date",$scope.dt);
                note.save(null, {
                    success: function (post) {
                        // Find all posts by the current user
                        var query = new Parse.Query(Note);
                        query.equalTo("objectId", post.id);
                        query.find({
                            success: function (userNotes) {
                                self.on.initialize()
                                $scope.$apply()
                                var currentNumber = $("#numberTask").html()
                                $("#numberTask").html(parseInt(currentNumber) + parseInt(1))

                            }
                        });

                    }
                });

                self.modelNote.title = null;
                growl.info("Task has been added.");

            }
        }

        self.on.initialize();

        var tmpList = [];
        $scope.sortingLog = [];

        var tmpList = [];

        for (var i = 1; i <= 6; i++){
            tmpList.push({
                text: 'Item ' + i,
                value: i
            });
        }

        $scope.list = tmpList;


        $scope.sortingLog = [];

        $scope.sortableOptions = {
            stop: function(e, ui) {
                console.log("stop");

                console.log(e)
                console.log(ui)
            }
        };


        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };


        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];


  });
