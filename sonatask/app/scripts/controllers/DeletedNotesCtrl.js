'use strict';

/**
 * @ngdoc function
 * @name sonataskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sonataskApp
 */
angular.module('sonataskApp')
  .controller('DeletedNotesCtrl', function ($scope,$state,growl,$q) {
    var self = this;

        self.modelNote={
            title:null,
            description:null,
            objectId:null
        }

        self.on={
            initialize:function(){
                $scope.deletedNotes=[];
                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("user", currentUser);
                query.equalTo("visible", false);

                query.find({
                    success: function(userNotes) {
                        // userPosts contains all of the posts by the current user.
                        for (var i = 0; i < userNotes.length; i++) {
                            var objNote ={};
                            var object = userNotes[i];
                            objNote.id=object.id;
                            objNote.data=object.attributes
                            $scope.deletedNotes.push(objNote)

                            $scope.$apply()
                        }
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });
            },
            restore:function(note){
                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("objectId", note.note.id);
                query.first({
                    success: function(object) {

                        object.set("visible", true);
                        object.save();
                        $scope.$apply();
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                $("#"+note.note.id).parent().remove();

                var currentNumber = $("#numberNotes").html()
                var currentDeleteNumber = $("#deletedNotes").html()
                $("#numberNotes").html(parseInt(currentNumber)+parseInt(1))
                $("#deletedNotes").html(parseInt(currentDeleteNumber)-parseInt(1))

                growl.info("Note has been restored.");

            }
        }


        $scope.deletedNotes=[];

        var currentUser = Parse.User.current();
        self.on.initialize();


  });
