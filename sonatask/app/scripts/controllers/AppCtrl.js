'use strict';

/**
 * @ngdoc function
 * @name sonataskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sonataskApp
 */
angular.module('sonataskApp')
  .controller('AppCtrl', function ($scope,$state,growl) {
    var self = this;
	$scope.mainView="app";
        self.numberNotes=0;

        // check if user is logged
        var currentUser = Parse.User.current();
        if (currentUser) {
            self.username = currentUser.attributes.username;
        } else {
            // show the signup or login page
            $state.go("landing");
            return;
        }


        var Note = Parse.Object.extend("Notes");
        var query = new Parse.Query(Note);
        query.equalTo("user", currentUser);
        query.equalTo("visible", true);
        query.find({
            success: function(userNotes) {
                self.numberNotes=userNotes.length;
                $scope.$apply()
                // userPosts contains all of the posts by the current user.
            },
            error:function(){
                self.numberNotes=0;
            }
        });


        var Note = Parse.Object.extend("Notes");
        var query = new Parse.Query(Note);
        query.equalTo("user", currentUser);
        query.equalTo("visible", false);
        query.find({
            success: function(userNotes) {
                self.deletedNotes=userNotes.length;
                $scope.$apply()
                // userPosts contains all of the posts by the current user.
            },
            error:function(){
                self.deletedNotes=0;
            }
        });

        var Note = Parse.Object.extend("Task");
        var query = new Parse.Query(Note);
        query.equalTo("user", currentUser);
        query.find({
            success: function(userNotes) {
                self.numberTask=userNotes.length;
                $scope.$apply()
                // userPosts contains all of the posts by the current user.
            },
            error:function(){
                self.numberTask=0;
            }
        });




        self.on={
            logout:function(){
                var currentUser = Parse.User.current();
                if (currentUser) {
                    Parse.User.logOut();
                    $state.go("landing");
                } else {
                    $state.go("landing");
                }
            }
        }





    });
