'use strict';

/**
 * @ngdoc function
 * @name sonataskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sonataskApp
 */
angular.module('sonataskApp')
  .controller('NotesCtrl', function ($scope,$state,growl,$q,FileUploader) {
    var self = this;

        self.modelNote={
            title:null,
            description:null,
            objectId:null,
            color:"white",
            url:null
        }

        self.on={
            initialize:function(){
                $scope.notes=[];
                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("user", currentUser);
                query.equalTo("visible", true);

                query.find({
                    success: function(userNotes) {
                        // userPosts contains all of the posts by the current user.
                        for (var i = 0; i < userNotes.length; i++) {
                            var objNote ={};
                            var object = userNotes[i];
                            objNote.id=object.id;
                            objNote.data=object.attributes
                            $scope.notes.push(objNote)

                            $scope.$apply()
                        }
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });
            },
            saveNote:function(){
                var noteTitle =self.modelNote.title
                var noteDescription = self.modelNote.description
                var color=self.modelNote.color;
                var image = self.modelNote.url;
                console.log(noteTitle)

                if(noteTitle == null || noteDescription == null){
                    growl.error("Please check form");
                    return
                }

                // Make a new post
                var Note = Parse.Object.extend("Notes");
                var note = new Note();
                note.set("name", noteTitle);
                note.set("description", noteDescription);
                note.set("user", currentUser);
                note.set("visible",true);
                note.set("color",color);
                note.set("image", image)
                note.save(null, {
                    success: function(post) {
                        // Find all posts by the current user
                        var query = new Parse.Query(Note);
                        query.equalTo("objectId", post.id);
                        query.find({
                            success: function(userNotes) {
                                self.on.initialize()
                                $scope.$apply()
                                var currentNumber = $("#numberNotes").html()
                                $("#numberNotes").html(parseInt(currentNumber)+parseInt(1))

                            }
                        });

                    }
                });

                self.modelNote.title =null;
                self.modelNote.description=null;
                self.modelNote.url=null;
                self.modelNote.color="white";
                growl.info("Note has been added.");

            },
            deleteNote:function(note){

                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("objectId", note.note.id);
                query.first({
                    success: function(object) {

                        object.set("visible", false);
                        object.save();
                        $scope.$apply();
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                $("#"+note.note.id).parent().remove();

                var currentNumber = $("#numberNotes").html()
                var currentDeleteNumber = $("#deletedNotes").html()
                $("#numberNotes").html(parseInt(currentNumber)-parseInt(1))
                $("#deletedNotes").html(parseInt(currentDeleteNumber)+parseInt(1))

                growl.warning("Note has been deleted.");
                $scope.$apply()

            },
            editNote:function(note){
                self.editing=true;
                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("objectId", note.note.id);
                query.first({
                    success: function(object) {
                        console.log(object)
                        self.modelNote.title=object.attributes.name;
                        self.modelNote.description=object.attributes.description;
                        self.modelNote.objectId=object.id;
                        self.modelNote.color=object.attributes.color;
                        $scope.$apply()
                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

            },
            editNoteSave:function(){
                var Note = Parse.Object.extend("Notes");
                var query = new Parse.Query(Note);
                query.equalTo("objectId",  self.modelNote.objectId);
                query.first({
                    success: function(object) {

                        object.set("name", self.modelNote.title);
                        object.set("description", self.modelNote.description);
                        object.set("color", self.modelNote.color);
                        object.save();
                        $scope.$apply()
                        self.editing=null;
                        self.modelNote.objectId=null;
                        self.modelNote.title =null;
                        self.modelNote.description=null;
                        self.modelNote.color="white";
                        growl.info("Note has been updated.");
                        $scope.$apply()
                        self.on.initialize()

                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

            },
            changeWhite:function(){
                self.modelNote.color="#fff";
            },
            changeBlue:function(){
                self.modelNote.color="#58D3F7";
            },
            changeGreen:function(){
                self.modelNote.color="#04B431";
            },
            changePink:function(){
                self.modelNote.color="#F6CEF5";
            },
            changeYellow:function(){
                self.modelNote.color="#F3F781";
            },
            changeBrown:function(){
                self.modelNote.color="#8A4B08";

            }
        }


        $scope.notes=[];

        var currentUser = Parse.User.current();
        self.on.initialize();
        $scope.fileName="";


        // FILE UPLOADER

        var uploader = $scope.uploader = new FileUploader({
            url: 'https://api.parse.com/1/files/'+$scope.fileName,
            autoUpload:true,
            withCredentials: false,
            headers: {
                'X-Parse-Application-Id': 'F03K1llrWjA0RGLVOUYdqTMVr4P9Qg9wabVAvDHp',
                'X-Parse-REST-API-Key': '8YxyadTlTCeqeGUfzwbyhYxDVQrqpiAht0zgP80n',
                'Content-Type': 'image/png'
            }
        });

        // FILTERS

        uploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
           // console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
           console.log(fileItem)
            $scope.fileName =fileItem.file.name
            $scope.fileType =fileItem.file.type
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            //console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            $scope.fileName =item._file.name
            $scope.fileType =item._file.type
           // console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            //console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            //console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
            self.modelNote.url=response.url
            console.log(response)
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };




  });
